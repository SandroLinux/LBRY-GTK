# <img src="./share/icons/hicolor/scalable/apps/lbry-gtk.svg" width="10%" height="auto"> LBRY-GTK

LBRY-GTK as the name may suggest is a GTK client to the LBRY network. LBRY-GTK in its early stages was a fork of FastLBRY-Terminal, however the code was modified a lot in order to become a full GTK GUI app. LBRY-GTK is also:
* A desktop (not webbrowser) app for doing a variety of things on the LBRY network
* Cross platform (For now Windows & Linux only, contributions welcome)
* Feature rich (Please make issues or talk in the matrix chat if something is missing)
* FOSS (Free as in Freedom) software

## Contribution:

LBRY-GTK has a lot of feature and bugfix ideas. All of these are listed in the [TODO.md file](TODO.md). Any help in terms of development or suggestions of what can be added/fixed is very much appreciated. For contributing code, check out the [CONTRIBUTING.md file](CONTRIBUTING.md).

## Links:

Questions and chat about the project: [Matrix](https://matrix.to/#/#LBRY-GTK:matrix.org)

AUR package: [lbry-gtk-git](https://aur.archlinux.org/packages/lbry-gtk-git)

Based on: [FastLBRY-terminal](https://notabug.org/jyamihud/FastLBRY-terminal)

Other great LBRY projects: [Awesome-LBRY](https://github.com/LBRYFoundation/Awesome-LBRY)

## List of features:
  * Lists, with thumbnails
    * Search
    * Advanced Search
    * Following
    * Your Tags
    * Discover
    * Library
    * Collections
    * Followed
    * Uploads
    * Channels
  * Wallet
    * Statistics
    * History
  * Open publication pages
    * Channel
    * Video
    * Audio
    * Article
    * Image
    * Collection
  * Open publications in browser
    * [Odysee](https://odysee.com/)
    * [Madiator](https://madiator.com/)
    * [Librarian](https://librarian.bcow.xyz/)
  * Comment as any of your channels
    * Open (with markdown support)
    * Write
    * Edit
    * Delete
    * Reply to
    * React to
  * Channel
    * Follow
    * Unfollow
    * Get RSS links
      * [Odysee](https://odysee.com/)
      * [Librarian](https://librarian.bcow.xyz/)
  * Make publications
    * Video
    * Audio
    * Article
    * Image
  * Search tags from publication page
  * Go back and forth
  * Display style
    * Grid
    * List
  * Extensive settings system
  * Specify commands per stream type
    * Video
    * Audio
    * Image
    * Document
  * Advanced Search
    * Text
    * Channels
    * Claims
    * Date
      * Simple
      * Custom
    * Claim Type
    * Stream Type
    * Tags
  * Order by
    * Release Time
    * Name
    * Trending
  * Order Direction
    * Descending
    * Ascending
  * Markdown display (set Document Command to LBRY-GTK-Document)

## Screenshots:

![](./Images/0.png)
![](./Images/1.png)
![](./Images/2.png)
![](./Images/3.png)
![](./Images/4.png)
![](./Images/5.png)
![](./Images/6.png)
![](./Images/7.png)
![](./Images/8.png)
![](./Images/9.png)
![](./Images/10.png)
![](./Images/11.png)
