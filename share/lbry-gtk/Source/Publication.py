################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, time, os, re, queue, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gdk

from flbry import url, channel

from Source import Places, Settings, Move
from Source.Open import Open
from Source.FetchComment import FetchComment
from Source.Error import Error
from Source.Thumbnail import Thumbnail
from Source.Document import Document
from Source.Tag import Tag
from Source.PublicationControl import PublicationControl
from Source.Markdown import Markdown


def Replace(Match):
    String = Match.string[Match.start() : Match.end()]
    if Match.string[Match.end() - 1] == "\n":
        return "[%s](%s)" % (String, String) + "\n"
    return "[%s](%s)" % (String, String)


class Publication:
    def __init__(self, *args):
        (
            self.Builder,
            self.ShowHiderTexts,
            self.Window,
            self.Stater,
            self.Title,
            self.MainSpace,
            self.AddPage,
        ) = args

        self.Queue = queue.Queue()

        Builder = self.Builder

        self.Publication = Builder.get_object("Publication")
        self.DataView = Builder.get_object("DataView")
        self.Expanders = [
            Builder.get_object("TagsExpander"),
            Builder.get_object("DescriptionExpander"),
            Builder.get_object("LinksExpander"),
            Builder.get_object("CommentExpander"),
        ]
        self.ThumbnailDataBox = Builder.get_object("ThumbnailDataBox")
        self.DescriptionBox = Builder.get_object("DescriptionBox")
        self.DataStore = Builder.get_object("DataStore")
        self.TagBox = Builder.get_object("TagBox")
        self.Links = Builder.get_object("Links")
        self.PublicationControlBox = Builder.get_object("PublicationControlBox")
        self.DataAction = Builder.get_object("DataAction")
        self.TagsAction = Builder.get_object("TagsAction")
        self.DescriptionAction = Builder.get_object("DescriptionAction")
        self.LinksAction = Builder.get_object("LinksAction")
        self.CommentsAction = Builder.get_object("CommentsAction")
        self.MoveLeft = Builder.get_object("MoveLeft")
        self.MoveRight = Builder.get_object("MoveRight")
        self.MoveUp = Builder.get_object("MoveUp")
        self.MoveDown = Builder.get_object("MoveDown")
        self.Activate1 = Builder.get_object("Activate1")
        self.Activate2 = Builder.get_object("Activate2")
        self.Activate3 = Builder.get_object("Activate3")

        self.DataView.set_search_equal_func(self.SearchFunction)

        self.Documenter = Document(
            self.Window,
            self.GetPublication,
            self.Stater,
            self.ShowHiderTexts,
            self.Title,
            self.MainSpace,
            self.AddPage,
        )

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Markdown.glade")
        self.Markdowner = Markdown(
            Builder,
            self.Window,
            self.GetPublication,
            "",
            "",
            True,
            True,
            self.AddPage,
        )
        Builder.connect_signals(self.Markdowner)
        self.DescriptionBox.pack_start(self.Markdowner.Document, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Thumbnail.glade")
        self.Thumbnailer = Thumbnail(Builder, self.Window)
        Builder.connect_signals(self.Thumbnailer)
        self.ThumbnailDataBox.pack_start(
            self.Thumbnailer.Thumbnail, True, True, 0
        )

        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "PublicationControl.glade"
        )
        self.PublicationControler = PublicationControl(
            Builder,
            self.Window,
            self.GetPublication,
            self.Documenter,
            self.Links,
            self.Stater,
            self.AddPage,
        )
        Builder.connect_signals(self.PublicationControler)
        self.PublicationControlBox.pack_start(
            self.PublicationControler.PublicationControl, True, True, 0
        )

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.Tagger = Tag(Builder, False)
        Builder.connect_signals(self.Tagger)
        self.TagBox.pack_start(self.Tagger.Tag, True, True, 0)
        self.Tagger.AddPage = self.AddPage

        self.Commenter = FetchComment(
            self.Window,
            self.ShowHiderTexts,
            self.GetPublication,
            self.Stater,
            self.AddPage,
            self.Expanders[3],
        )

    def ShowLinks(self, Links):
        self.Links.forall(self.Links.remove)
        for Link in Links:
            LinkButton = Gtk.LinkButton.new_with_label(Link[1], Link[0])
            LinkButton.set_halign(Gtk.Align.START)
            self.Links.add(LinkButton)
        self.Links.show_all()

    def GetLinks(self, Url):
        Links = url.web(Url)
        GLib.idle_add(self.ShowLinks, Links)

    def GetTitle(self, Data):
        Title = ""
        try:
            Title = Data["signing_channel"]["value"]["title"] + " - "
        except:
            pass
        try:
            Title = Title + Data["value"]["title"]
        except:
            Title = Title + Data["name"][1:]
        return Title

    def GetPublication(self, Url, Check=False, State=False):
        self.Queue.put(True)

        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        Data = url.get([Url], server=Session["Server"])[0]
        if isinstance(Data, str):
            Error(Data, self.Window)
            return
        NewTitle = self.GetTitle(Data)
        if not State:
            self.Stater.Save(self.GetPublication, [Url], NewTitle)
        GLib.idle_add(self.ShowPublication, Data)

    def ShowPublication(self, Data):
        self.Replace("Publication")
        for Expander in self.Expanders:
            Expander.set_expanded(False)

        NewTitle = self.GetTitle(Data)

        ClaimID = Data["claim_id"]

        try:
            Channel = Data["signing_channel"]["permanent_url"]
            ChannelID = Data["signing_channel"]["claim_id"]
        except:
            Channel = Data["permanent_url"]
            ChannelID = Data["claim_id"]

        self.PublicationControler.ShowPublicationControl(Data, NewTitle)

        self.Title.set_text(NewTitle)

        # This seems to be overly complicated
        Unneeded = ["value_description", "value_tags"]
        Tocheck = [Data]
        TocheckNames = ["data"]
        Branches = {"data": None}
        BranchPlaces = {str(Data): None}
        i = 0
        self.DataStore.clear()
        while i < len(Tocheck):
            Branch = Tocheck[i]
            BranchName = TocheckNames[i]
            if BranchName != "data":
                Branches[BranchName] = self.DataStore.append(
                    BranchPlaces[str(Branch)], (str(BranchName), "")
                )
            if isinstance(Branch, dict):
                Iter = Branch.items()
            else:
                Iter = enumerate(Branch)
            for Key, Value in Iter:
                if isinstance(Value, dict) or isinstance(Value, list):
                    BranchPlaces[str(Value)] = Branches[BranchName]
                    TocheckNames.append(Key)
                    Tocheck.append(Value)
                else:
                    if not (str(BranchName) + "_" + str(Key)) in Unneeded:
                        self.DataStore.append(
                            Branches[BranchName], (str(Key), str(Value))
                        )
            i += 1

        try:
            Description = str(Data["value"]["description"])
        except:
            Description = "No description"

        Url = "(?<!\]\()((?:https?|ftp):\/\/[^\s\]\)]*)(?:[\s\]\)](?!\()|$)"
        TitleUrl = "\[(.*?)\]\((\S*?) ?([\"'](.*?)[\"'])?\)"

        Description = re.sub(TitleUrl, "[\\1](\\2)", Description)
        Description = re.sub("#([^# ])", "\\#\\1", Description)
        Description = Description.replace("\n", "\n\n")

        self.Markdowner.Text = re.sub(Url, Replace, Description)
        self.Markdowner.Fill()

        self.Tagger.RemoveAll()
        try:
            self.Tagger.Append(Data["value"]["tags"])
        except:
            pass

        TagFlowChildren = len(self.Tagger.Tags)
        if TagFlowChildren == 0:
            self.Expanders[0].set_label("No Tags")
            self.Tagger.Add("I told you")
        else:
            self.Expanders[0].set_label("Tags (" + str(TagFlowChildren) + ")")

        try:
            self.Thumbnailer.Url = Data["value"]["thumbnail"]["url"]
        except:
            self.Thumbnailer.Url = ""

        args = [Data["canonical_url"]]
        threading.Thread(target=self.GetLinks, args=(args)).start()

        del self.ShowHiderTexts[:]
        self.Publication.show_all()
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        Session = LBRYSettings["Session"]
        LBRYSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        if LBRYSettings["EnableComments"]:
            ChannelList = channel.channel_list(server=Session["Server"])
            CommentServer = LBRYSettings["CommentServer"]
            self.Expanders[3].show()
            CommentBox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
            self.Commenter.SingleComment(
                ClaimID,
                CommentBox,
                [0, 0, "", "", "", "", "", 0, 0, 0, False, False, False],
                {"Root": CommentBox},
                CommentServer,
                True,
                False,
                LBRYSettings,
                ChannelList,
                Channel,
            )
            self.Expanders[3].remove(self.Expanders[3].get_children()[0])
            self.Expanders[3].add(CommentBox)
            self.Queue.put(True)
            self.Queue = queue.Queue()
            Args = [
                ClaimID,
                CommentBox,
                self.Queue,
                CommentServer,
                Channel,
            ]
            threading.Thread(
                target=self.Commenter.Fetch, args=(Args), daemon=True
            ).start()
            self.Expanders[3].set_label("Comments")
            Args = [self.Expanders[3], ChannelID, CommentServer]
            threading.Thread(
                target=self.Commenter.DisplayPrice, args=(Args)
            ).start()
        else:
            self.Expanders[3].hide()

    def SearchFunction(self, Model, Column, Key, Iter):
        Row = Model[Iter]
        if Key.lower() in list(Row)[Column].lower():
            return False

        for Inner in Row.iterchildren():
            if Key.lower() in list(Inner)[Column].lower():
                self.DataView.expand_to_path(Row.path)
                break
        else:
            self.DataView.collapse_row(Row.path)
        return True

    def ChangeExpander(self, Index):
        for ExpanderIndex in range(len(self.Expanders)):
            self.Expanders[ExpanderIndex].set_expanded(ExpanderIndex == Index)

    def on_DataAction_activate(self, Widget):
        self.ChangeExpander(4)
        self.DataView.get_selection().select_path(0)
        self.DataView.grab_focus()

    def on_TagsAction_activate(self, Widget):
        self.ChangeExpander(0)
        self.Tagger.FlowBox.get_children()[0].grab_focus()

    def on_DescriptionAction_activate(self, Widget):
        self.ChangeExpander(1)

    def on_LinksAction_activate(self, Widget):
        self.ChangeExpander(2)

    def on_CommentsAction_activate(self, Widget):
        self.ChangeExpander(3)

    def on_MoveLeft_activate(self, Widget):
        if not self.Publication.get_realized():
            return
        if self.Expanders[0].get_expanded():
            Move.FlowBoxLeftRight(self.Tagger.FlowBox, -1)

    def on_MoveRight_activate(self, Widget):
        if not self.Publication.get_realized():
            return
        if self.Expanders[0].get_expanded():
            Move.FlowBoxLeftRight(self.Tagger.FlowBox)

    def on_MoveDown_activate(self, Widget):
        if self.Publication.get_realized():
            if self.Expanders[0].get_expanded():
                Move.FlowBoxUpDown(self.Tagger.FlowBox)
            elif self.Expanders[1].get_expanded():
                self.Markdowner.SelectNext()
            elif self.DataView.get_realized():
                Move.ListViewUpDown(self.DataView)
                Path = self.DataView.get_selection().get_selected_rows()[1][0]
                self.DataView.scroll_to_cell(Path, None, False, 0, 0)
        elif self.Documenter.Markdowner.Document.get_realized():
            self.Documenter.Markdowner.SelectNext()

    def on_MoveUp_activate(self, Widget):
        if self.Publication.get_realized():
            if self.Expanders[0].get_expanded():
                Move.FlowBoxUpDown(self.Tagger.FlowBox, -1)
            elif self.Expanders[1].get_expanded():
                self.Markdowner.SelectPrevious()
            elif self.DataView.get_realized():
                Move.ListViewUpDown(self.DataView, -1)
                Path = self.DataView.get_selection().get_selected_rows()[1][0]
                self.DataView.scroll_to_cell(Path, None, False, 0, 0)
        elif self.Documenter.Markdowner.Document.get_realized():
            self.Documenter.Markdowner.SelectPrevious()

    def on_Activate1_activate(self, Widget):
        if self.Publication.get_realized():
            if self.Expanders[0].get_expanded():
                self.Tagger.on_Search_button_press_event(
                    "", "", Gdk.BUTTON_PRIMARY
                )
            elif self.Expanders[1].get_expanded():
                self.Markdowner.Activate(Gdk.BUTTON_PRIMARY)
            elif self.DataView.get_realized():
                try:
                    Selection = self.DataView.get_selection()
                    Path = Selection.get_selected_rows()[1][0]
                    self.DataView.expand_row(Path, False)
                    Path.append_index(0)
                    Selection.select_path(Path)
                except:
                    pass
        elif self.Documenter.Markdowner.Document.get_realized():
            self.Documenter.Markdowner.Activate(Gdk.BUTTON_PRIMARY)

    def on_Activate2_activate(self, Widget):
        if self.Publication.get_realized():
            if self.Expanders[0].get_expanded():
                self.Tagger.on_Search_button_press_event(
                    "", "", Gdk.BUTTON_MIDDLE
                )
            elif self.Expanders[1].get_expanded():
                self.Markdowner.Activate(Gdk.BUTTON_MIDDLE)
            elif self.DataView.get_realized():
                try:
                    Selection = self.DataView.get_selection()
                    Path = Selection.get_selected_rows()[1][0]
                    Path.up()
                    Selection.select_path(Path)
                    self.DataView.collapse_row(Path)
                except:
                    pass
        elif self.Documenter.Markdowner.Document.get_realized():
            self.Documenter.Markdowner.Activate(Gdk.BUTTON_MIDDLE)

    def on_Activate3_activate(self, Widget):
        if not self.Publication.get_realized():
            return
        if self.Expanders[0].get_expanded():
            self.Tagger.Select()
        elif self.DataView.get_realized():
            try:
                Path = self.DataView.get_selection().get_selected_rows()[1][0]
                Column = self.DataView.get_column(1)
                self.DataView.set_cursor(Path, Column, True)
            except:
                pass
