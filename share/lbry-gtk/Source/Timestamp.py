################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import calendar, datetime, time

# This file deals with Epoch/Posix timestamps


def CurrentTime():

    # Get the current time in Posix time
    TimeNow = datetime.datetime.now()
    TimeStampNow = datetime.datetime.timestamp(TimeNow)
    TimeStampNow = int(TimeStampNow)

    return TimeStampNow


def Ago(TimeStamp):

    TimeStampNow = CurrentTime()

    # Sometimes comment might have earlier timestamp
    # than what the actual time is when posted early
    if TimeStampNow <= TimeStamp:
        return "Just now"

    Ago = 0
    Sec = 60
    Day = Sec * Sec * 24

    # Levels of periods from year to second
    Periods = [
        [Day * 365, "Y", " year"],
        [Day * 31, "m", " month"],
        [Day * 7, "W", " week"],
        [Day, "j", " day"],
        [Sec * Sec, "H", " hour"],
        [Sec, "M", " minute"],
        [0, "S", " second"],
    ]

    Ago = TimeStampNow - TimeStamp

    # This checks how long ago
    for TimePeriod in Periods:
        if Ago >= TimePeriod[0]:
            Form = TimePeriod[1]
            Period = TimePeriod[2]
            break

    if Form == "m":
        Ago = Ago - Day * 31
    if Form == "j":
        Ago = Ago - Day

    X = datetime.datetime.fromtimestamp(Ago, datetime.timezone.utc)
    TimeAgo = int(X.strftime("%" + Form))

    if Form == "Y":
        TimeAgo = TimeAgo - 1970

    # Here we make it in plural if needed
    if TimeAgo > 1:
        Period += "s"
    Period += " ago "

    # PROBABLY a good idea to bring "GetDate" here too
    # and send it in array with this one?
    return str(TimeAgo) + Period


def GetDate(TimeStamp):
    DateIs = datetime.datetime.fromtimestamp(TimeStamp)
    return DateIs.strftime("%Y-%m-%d %H:%M:%S")
