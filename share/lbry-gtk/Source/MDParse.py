################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import re, markdown, gi, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, Gdk

from flbry import settings

from Source import Image, Places
from Source.Open import Open
from Source.Error import Error


def Parse(Markdown):
    Text = markdown.markdown(Markdown, extensions=["fenced_code"])
    Text = re.sub(' class=".*"', "", Text)
    Text = re.sub(' alt="[^"]*"', "", Text)
    Text = re.sub(' width="[^"]*"', "", Text)
    Text = re.sub(' height="[^"]*"', "", Text)
    Text = re.sub("&quot;", '"', re.sub("&apos;", "'", Text))
    Text = re.sub("&amp;", "&", Text)

    NoPs = ["li", "blockquote"]
    for NoP in NoPs:
        Text = re.sub(
            "(<" + NoP + ">\n<p>)([\s\S]*?(?=</p))</p>",
            "<" + NoP + ">\\2",
            Text,
        )
    Text = re.sub("<blockquote>", "<br/><blockquote>", Text)

    Text = re.sub("</p>", "<br/>", re.sub("<p[^>]*>", "<br/>", Text))
    Text = re.sub("<h1>", "<br/><big><big><big><big><big><big>", Text)
    Text = re.sub("</h1>", "</big></big></big></big></big></big><br/>", Text)
    Text = re.sub("<h2>", "<br/><big><big><big><big><big>", Text)
    Text = re.sub("</h2>", "</big></big></big></big></big><br/>", Text)
    Text = re.sub("<h3>", "<br/><big><big><big><big>", Text)
    Text = re.sub("</h3>", "</big></big></big></big><br/>", Text)
    Text = re.sub("<h4>", "<br/><big><big><big>", Text)
    Text = re.sub("</h4>", "</big></big></big><br/>", Text)
    Text = re.sub("<h5>", "<br/><big><big>", Text)
    Text = re.sub("</h5>", "</big></big><br/>", Text)
    Text = re.sub("</h6>", "</big><br/>", re.sub("<h6>", "<br/><big>", Text))
    Text = re.sub("</strong>", "</b>", re.sub("<strong>", "<b>", Text))
    Text = re.sub("</em>", "</i>", re.sub("<em>", "<i>", Text))
    Text = re.sub("</code>", "</tt>", re.sub("<code>", "<tt>", Text))
    Text = re.sub("</li>", "", Text)
    Text = re.sub("<img", " <img", Text)

    Text = re.sub("</module>", "", re.sub("<module>", "", Text))
    Text = re.sub("</click>", "", re.sub("<click>", "", Text))
    Text = re.sub("</stkr>", "", re.sub("<stkr>", "", Text))
    Text = re.sub("</center>", "", re.sub("<center>", "", Text))
    Text = re.sub("</pre>", "", re.sub("<pre>", "", Text))
    Text = re.sub("</font>", "", re.sub("<font[^>]*>", "", Text))

    Text = re.sub("    ", "\t", Text)
    Lines = Text.split("\n")

    CodeBlock = False
    ListLevel = -1
    ListTypes = []
    Counters = []
    for Index in range(len(Lines)):
        if "<tt>" in Lines[Index] and not "</tt>" in Lines[Index]:
            CodeBlock = True
            Lines[Index] += "</tt><br/>"
            continue
        if CodeBlock:
            Lines[Index] = "<tt>" + Lines[Index]
            if "</tt>" in Lines[Index]:
                CodeBlock = False
            else:
                Lines[Index] += "</tt><br/>"
        else:
            if re.match("^(\t*)-", Lines[Index]):
                Lines[Index] = re.sub("^(\t*)-", "<br/>\\1•", Lines[Index])
            if re.match("^(\t*)([0-9]+)", Lines[Index]):
                Lines[Index] = re.sub(
                    "^(\t*)([0-9]+)", "<br/>\\1\\2", Lines[Index]
                )
            if (
                re.match("^=+$", Lines[Index])
                and Index != 0
                and not "<big>" in Lines[Index - 1]
            ):
                Lines[Index - 1] = (
                    "<br/><big><big><big><big><big><big>"
                    + Lines[Index - 1]
                    + "</big></big></big></big></big></big>"
                )
                Lines[Index] = ""
            if (
                re.match("^-+$", Lines[Index])
                and Index != 0
                and not "<big>" in Lines[Index - 1]
            ):
                Lines[Index - 1] = (
                    "<br/><big><big><big><big><big>"
                    + Lines[Index - 1]
                    + "</big></big></big></big></big>"
                )
                Lines[Index] = ""
            if (
                "<ul>" in Lines[Index]
                and "<li>" in Lines[Index]
                and Lines[Index].find("<li>") < Lines[Index].find("<ul>")
            ):
                Sign = "• "
                if ListTypes[-1] == "O":
                    Sign = str(Counters[-1]) + ". "
                    Counters[-1] += 1
                Lines[Index] = re.sub(
                    "<li>", ListLevel * "\t" + Sign, Lines[Index]
                )
                ListLevel += 1
                Lines[Index] = re.sub("<ul>", "", Lines[Index])
                Lines[Index] = "<br/>" + Lines[Index]
                ListTypes.append("U")
            elif (
                "<ol>" in Lines[Index]
                and "<li>" in Lines[Index]
                and Lines[Index].find("<li>") < Lines[Index].find("<ol>")
            ):
                Sign = "• "
                if ListTypes[-1] == "O":
                    Sign = str(Counters[-1]) + ". "
                    Counters[-1] += 1
                Lines[Index] = re.sub(
                    "<li>", ListLevel * "\t" + Sign, Lines[Index]
                )
                ListLevel += 1
                Lines[Index] = re.sub("<ol>", "", Lines[Index])
                Lines[Index] = "<br/>" + Lines[Index]
                ListTypes.append("O")
                Counters.append(1)
            elif "<ul>" in Lines[Index]:
                ListTypes.append("U")
                ListLevel += 1
                Lines[Index] = re.sub("<ul>", "", Lines[Index])
            elif "<ol>" in Lines[Index]:
                ListTypes.append("O")
                Counters.append(1)
                ListLevel += 1
                Lines[Index] = re.sub("<ol>", "", Lines[Index])
            elif "</ul>" in Lines[Index] or "</ol>" in Lines[Index]:
                if "</ol>" in Lines[Index]:
                    Counters.pop()
                ListTypes.pop()
                ListLevel -= 1
                Lines[Index] = re.sub(
                    "</ul>", "", re.sub("</ol>", "", Lines[Index])
                )
            if ListLevel != -1 and "<li>" in Lines[Index]:
                Sign = "• "
                if ListTypes[-1] == "O":
                    Sign = str(Counters[-1]) + ". "
                    Counters[-1] += 1
                Lines[Index] = re.sub(
                    "<li>", ListLevel * "\t" + Sign, Lines[Index]
                )
                Lines[Index] = "<br/>" + Lines[Index]
    return re.sub("<br\\s*/?>", "\n", " ".join(Lines)).strip()
