################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################


def FlowBoxLeftRight(FlowBox, Step=1):
    Children = FlowBox.get_children()
    try:
        Index = Children.index(FlowBox.get_selected_children()[0])
        Index += Step
        if -1 < Index:
            FlowBox.select_child(Children[Index])
    except:
        FlowBox.select_child(FlowBox.get_children()[0])


def FlowBoxUpDown(FlowBox, Direction=1):
    Children = FlowBox.get_children()
    Columns = 0
    for Child in Children:
        if Child.get_allocation().y == 0:
            Columns += 1
    FlowBoxLeftRight(FlowBox, Direction * Columns)


def ListViewUpDown(ListView, Direction=1):
    Selection = ListView.get_selection()
    try:
        Path = Selection.get_selected_rows()[1][0]
        if Direction == 1:
            Path.next()
        else:
            Path.prev()
        Selection.select_path(Path)
    except:
        Selection.select_path(0)
