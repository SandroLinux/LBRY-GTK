################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, time, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GdkPixbuf, GLib

from flbry import url

from Source import Settings, Places
from Source.Error import Error


class TopPanel:
    def __init__(self, *args):
        (
            self.Builder,
            self.Window,
            self.Logo,
            self.BackImage,
            self.NewPage,
            self.SamePage,
        ) = args
        self.TopPanel = self.Builder.get_object("TopPanel")
        self.LBRY = self.Builder.get_object("LBRY")
        self.Search = self.Builder.get_object("Search")
        self.Balance = self.Builder.get_object("Balance")
        self.New = self.Builder.get_object("New")
        self.Inbox = self.Builder.get_object("Inbox")
        self.NewPublication = self.Builder.get_object("NewPublication")
        self.Settings = self.Builder.get_object("Settings")
        self.Help = self.Builder.get_object("Help")
        self.Status = self.Builder.get_object("Status")
        self.About = self.Builder.get_object("About")
        self.ShowMenu = self.Builder.get_object("ShowMenu")
        self.MenuBox = self.Builder.get_object("MenuBox")
        self.MenuBar = self.Builder.get_object("MenuBar")
        self.Menu = self.Builder.get_object("Menu")

        Height = self.BackImage.get_preferred_height().minimum_height

        ScaledLogo = self.Logo.scale_simple(
            Height, Height, GdkPixbuf.InterpType.BILINEAR
        )
        Gtk.IconTheme.add_builtin_icon("LBRY-GTK", -1, ScaledLogo)
        self.LBRY.set_image(
            Gtk.Image.new_from_icon_name("LBRY-GTK", Gtk.IconSize.BUTTON)
        )

    def on_NewPublication_activate(self, Widget):
        self.on_NewPublication_button_press_event(
            Widget, "", Gdk.BUTTON_PRIMARY
        )

    def on_Settings_activate(self, Widget):
        self.on_Settings_button_press_event(Widget, "", Gdk.BUTTON_PRIMARY)

    def on_Help_activate(self, Widget):
        self.on_Help_button_press_event(Widget, "", Gdk.BUTTON_PRIMARY)

    def on_Status_activate(self, Widget):
        self.on_Status_button_press_event(Widget, "", Gdk.BUTTON_PRIMARY)

    def on_Balance_activate(self, Widget):
        self.on_Balance_button_press_event(Widget, "", Gdk.BUTTON_PRIMARY)

    def on_Inbox_activate(self, Widget):
        Widget.set_active(not Widget.get_active())
        self.on_Inbox_button_press_event(Widget, "", Gdk.BUTTON_PRIMARY)

    def on_ShowMenu_activate(self, Widget):
        if len(self.MenuBox.get_children()) != 0:
            self.MenuBar.select_first(False)
        else:
            self.Menu.activate()
            threading.Thread(target=self.ShowMenuThread, args=([])).start()

    def ShowMenuThread(self):
        while not self.Menu.get_active():
            time.sleep(0.01)
        GLib.idle_add(self.ShowMenuUpdate)

    def ShowMenuUpdate(self):
        self.MenuBar.select_first(False)

    def on_LBRY_clicked(self, Widget):
        self.Search.grab_focus()
        self.Search.set_text("lbry://")
        self.Search.set_position(7)

    def on_Balance_button_press_event(self, Widget, Event, Button=""):
        if Button == "":
            Button = Event.button
        if self.Startuper.Started:
            if Button == Gdk.BUTTON_PRIMARY:
                self.SamePage("Advanced Search", ["Wallet", "Wallet", "Wallet"])
            elif Button == Gdk.BUTTON_MIDDLE:
                self.NewPage("Advanced Search", ["Wallet", "Wallet", "Wallet"])
        else:
            Error("LBRYNet is not running.", self.Window)

    def on_Inbox_button_press_event(self, Widget, Event, Button=""):
        if Button == "":
            Button = Event.button
        if self.Startuper.Started:
            if Button == Gdk.BUTTON_PRIMARY:
                self.SamePage("Inbox", [])
            elif Button == Gdk.BUTTON_MIDDLE:
                self.NewPage("Inbox", [])
        else:
            Error("LBRYNet is not running.", self.Window)

    def on_Inbox_pressed(self, Widget):
        Widget.set_active(True)

    def on_Inbox_released(self, Widget):
        Widget.set_active(False)

    def on_Search_key_press_event(self, Widget, Event):
        Search = Widget.get_text()
        if Gdk.keyval_name(Event.keyval) == "Return" and Search != "":
            if self.Startuper.Started:
                NewTitle = "Search: " + Search
                with open(Places.ConfigDir + "Session.json", "r") as File:
                    Session = json.load(File)
                if Search.startswith("lbry://") and not isinstance(
                    url.get([Search], server=Session["Server"])[0], str
                ):
                    self.SamePage("Publication", [Search])
                else:
                    self.SamePage(
                        "Advanced Search",
                        ["Search", "Content", NewTitle, {"text": Search}],
                    )
            else:
                Error("LBRYNet is not running.", self.Window)

    def on_About_activate(self, Widget):
        self.Builder.add_from_file(Places.GladeDir + "About.glade")
        AboutWindow = self.Builder.get_object("AboutWindow")
        AboutWindow.set_logo(self.Logo)
        AboutWindow.set_icon(self.Logo)
        AboutWindow.run()
        AboutWindow.destroy()

    def on_NewPublication_button_press_event(self, Widget, Event, Button=""):
        if Button == "":
            Button = Event.button
        if self.Startuper.Started:
            if Button == Gdk.BUTTON_PRIMARY:
                self.SamePage("NewPublication", [])
            elif Button == Gdk.BUTTON_MIDDLE:
                self.NewPage("NewPublication", [])
        else:
            Error("LBRYNet is not running.", self.Window)

    def on_Settings_button_press_event(self, Widget, Event, Button=""):
        if Button == "":
            Button = Event.button
        if Button == Gdk.BUTTON_PRIMARY:
            self.SamePage("Settings", [])
        elif Button == Gdk.BUTTON_MIDDLE:
            self.NewPage("Settings", [])

    def on_Help_button_press_event(self, Widget, Event, Button=""):
        if Button == "":
            Button = Event.button
        if Button == Gdk.BUTTON_PRIMARY:
            self.SamePage("Help", [])
        elif Button == Gdk.BUTTON_MIDDLE:
            self.NewPage("Help", [])

    def on_Status_button_press_event(self, Widget, Event, Button=""):
        if Button == "":
            Button = Event.button
        if Button == Gdk.BUTTON_PRIMARY:
            self.SamePage("Status", [])
        elif Button == Gdk.BUTTON_MIDDLE:
            self.NewPage("Status", [])

    # Not Yet Implemented

    def on_NewChannel_activate(self, menuitem):
        This = "is not working yet"

    def on_CreatorAnalytics_activate(self, menuitem):
        This = "is not working yet"

    def on_Rewards_activate(self, menuitem):
        This = "is not working yet"

    def on_Invites_activate(self, menuitem):
        This = "is not working yet"

    def on_SignOut_activate(self, menuitem):
        This = "is not working yet"
