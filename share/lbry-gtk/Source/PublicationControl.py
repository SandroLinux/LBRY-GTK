################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, time, os, json, shutil

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gdk, Gtk

from flbry import following, main, settings, error, channel, collection, support

from Source import Places, Settings
from Source.Error import Error
from Source.Open import Open
from Source.Channels import Channels

Types = [Gdk.BUTTON_PRIMARY, Gdk.BUTTON_MIDDLE]


class PublicationControl:
    def __init__(self, *args):
        (
            self.Builder,
            self.Window,
            self.GetPublication,
            self.Documenter,
            self.Links,
            self.Stater,
            self.AddPage,
        ) = args
        self.PublicationControl = self.Builder.get_object("PublicationControl")
        self.TypeDependent = self.Builder.get_object("TypeDependent")
        self.UnFollow = self.Builder.get_object("UnFollow")
        self.Download = self.Builder.get_object("Download")
        self.Play = self.Builder.get_object("Play")
        self.Link = self.Builder.get_object("Link")
        self.Repost = self.Builder.get_object("Repost")
        self.Boost = self.Builder.get_object("Boost")
        self.Tip = self.Builder.get_object("Tip")
        self.RSS = self.Builder.get_object("RSS")
        self.Buttons = [
            self.Play,
            self.Builder.get_object("Channel"),
            self.Builder.get_object("Content"),
        ]
        self.PlayOnNew = self.Builder.get_object("PlayOnNew")
        self.PlayOnCurrent = self.Builder.get_object("PlayOnCurrent")
        self.ContentOnNew = self.Builder.get_object("ContentOnNew")
        self.ContentOnCurrent = self.Builder.get_object("ContentOnCurrent")
        self.ChannelOnNew = self.Builder.get_object("ChannelOnNew")
        self.ChannelOnCurrent = self.Builder.get_object("ChannelOnCurrent")

    def Activate(self, Button, Type=0):
        if not self.Buttons[Button].get_realized():
            return
        Event = Gdk.Event.new(Gdk.EventType.BUTTON_PRESS)
        Event.button = Types[Type]
        Event.window = self.Buttons[Button].get_window()
        self.Buttons[Button].event(Event)

    def on_PlayOnNew_activate(self, Widget):
        self.Activate(0, 1)

    def on_PlayOnCurrent_activate(self, Widget):
        self.Activate(0)

    def on_ContentOnNew_activate(self, Widget):
        self.Activate(2, 1)

    def on_ContentOnCurrent_activate(self, Widget):
        self.Activate(2)

    def on_ChannelOnNew_activate(self, Widget):
        self.Activate(1, 1)

    def on_ChannelOnCurrent_activate(self, Widget):
        self.Activate(1)

    def GetFollow(self):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        IfFollow = following.if_follow(self.Channel, server=Session["Server"])
        GLib.idle_add(self.SetFollow, IfFollow)

    def SetFollow(self, Follow):
        if Follow:
            self.UnFollow.set_label("Unfollow")
        else:
            self.UnFollow.set_label("Follow")
        self.UnFollow.show_all()

    def ShowPublicationControl(self, *args):
        self.Data, self.Title = args
        self.ValueType = self.Data["value_type"]
        self.Url = self.Data["canonical_url"]
        self.ChannelShort = self.Data["short_url"]

        if "fee" in self.Data["value"]:
            self.Download.set_label("Buy/Download")
            self.Play.set_label("Buy/Play")
        else:
            self.Download.set_label("Download")
            self.Play.set_label("Play")

        self.StreamType = ""
        try:
            self.StreamType = self.Data["value"]["stream_type"]
        except:
            pass

        try:
            self.Channel = self.Data["signing_channel"]["permanent_url"]
        except:
            self.Channel = self.Data["permanent_url"]

        for Widget in self.TypeDependent.get_children():
            if self.ValueType in Widget.get_name():
                Widget.show()
            else:
                Widget.hide()
        threading.Thread(target=self.GetFollow, args=([])).start()

    def on_UnFollow_clicked(self, Widget):
        if not Widget.get_realized():
            return
        threading.Thread(target=self.UnFollowHelper, args=([])).start()

    def UnFollowHelper(self):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        if self.UnFollow.get_label() == "Unfollow":
            if following.unfollow_channel(
                self.Channel, server=Session["Server"]
            ):
                GLib.idle_add(self.SetFollow, False)
        else:
            if following.follow_channel(self.Channel, server=Session["Server"]):
                GLib.idle_add(self.SetFollow, True)

    def on_Channel_button_press_event(self, Widget, Event):
        args = [self.Channel]
        if Event.button == Gdk.BUTTON_PRIMARY:
            threading.Thread(target=self.GetPublication, args=(args)).start()
        elif Event.button == Gdk.BUTTON_MIDDLE:
            self.AddPage(".", self.Stater.Export(self.GetPublication, args))

    def Confirmation(self):
        Dialog = Gtk.MessageDialog(buttons=Gtk.ButtonsType.OK_CANCEL)
        Dialog.props.text = (
            "Are you sure you want to resolve this publication? ("
            + self.Data["value"]["fee"]["amount"]
            + " "
            + self.Data["value"]["fee"]["currency"]
            + ")"
        )
        Response = Dialog.run()
        Dialog.destroy()
        return Response == Gtk.ResponseType.OK

    def on_Play_button_press_event(self, Widget, Event):
        if not Widget.get_label().startswith("Buy") or self.Confirmation():
            threading.Thread(
                target=self.ResolveHelper, args=([Event.button])
            ).start()

    def on_Download_clicked(self, Widget):
        if not Widget.get_realized():
            return
        if not Widget.get_label().startswith("Buy") or self.Confirmation():
            threading.Thread(target=self.ResolveHelper, args=([])).start()

    def ResolveHelper(self, Button=False):
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        Directory = ""
        if Button and not LBRYSettings["settings"]["save_files"]:
            Directory = Places.TmpDir
        JsonData = main.get(
            self.Url,
            "",
            Directory,
            save_file=True,
            server=LBRYSettings["Session"]["Server"],
        )

        if isinstance(JsonData, str):
            Error(JsonData, self.Window)
            return

        Path = JsonData["download_path"]

        Args = [
            Button,
            LBRYSettings["settings"]["save_files"],
            LBRYSettings["settings"]["download_dir"],
            JsonData["download_directory"],
            JsonData["download_path"],
            JsonData["stream_name"],
        ]

        threading.Thread(
            target=self.MoveThread, args=(Args), daemon=True
        ).start()

        if Button:
            Shared = LBRYSettings["preferences"]["shared"]["value"]
            Command = Shared["LBRY-GTK"]["PlayCommand"]
            if self.StreamType == "video":
                Command = Shared["LBRY-GTK"]["VideoCommand"]
            elif self.StreamType == "audio":
                Command = Shared["LBRY-GTK"]["AudioCommand"]
            elif self.StreamType == "image":
                Command = Shared["LBRY-GTK"]["ImageCommand"]
            elif self.StreamType == "document":
                Command = Shared["LBRY-GTK"]["DocumentCommand"]
            if self.StreamType == "document" and Command == "LBRY-GTK-Document":
                GLib.idle_add(self.DocumentUpdate, Path, Button)
            else:
                try:
                    Open(Path, Command)
                except Exception as e:
                    Error(error.error(e), self.Window)

    def MoveThread(
        self, Button, SaveFiles, Location, DownloadDirectory, Path, Name
    ):
        if (not Button) or (SaveFiles and Location != DownloadDirectory):
            Counter, Size = 0, 0
            while True:
                time.sleep(0.1)
                try:
                    NewSize = os.path.getsize(Path)
                    if Size == NewSize:
                        Counter += 1
                    else:
                        Size = NewSize
                        Counter = 0
                except:
                    Counter = 0
                if Counter == 10:
                    break
            if Location[-1] != os.sep:
                Location += os.sep
            shutil.move(Path, Location + Name)

    def DocumentUpdate(self, Path, Button):
        if Button == Gdk.BUTTON_PRIMARY:
            threading.Thread(
                target=self.Documenter.Display, args=([Path, self.Title])
            ).start()
        elif Button == Gdk.BUTTON_MIDDLE:
            self.AddPage(
                ".",
                self.Stater.Export(self.Documenter.Display, [Path, self.Title]),
            )

    def on_Content_button_press_event(self, Widget, Event):
        if self.ValueType == "collection":
            NewTitle = "Collection: " + self.Title
            LBRYSettings = Settings.Get()
            if isinstance(LBRYSettings, str):
                Error(self.Window, LBRYSettings)
                return []
            ClaimIds = []
            Page = 1
            while True:
                NewClaims = collection.resolve(
                    page=Page,
                    url=self.Url,
                    server=LBRYSettings["Session"]["Server"],
                )
                Page += 1
                for Claim in NewClaims:
                    ClaimIds.append(Claim[3])
                if len(NewClaims) != 5:
                    break
            if Event.button == Gdk.BUTTON_PRIMARY:
                threading.Thread(
                    target=self.ButtonThread,
                    args=(
                        ["Search", "Content", NewTitle, {"claim_ids": ClaimIds}]
                    ),
                ).start()
            elif Event.button == Gdk.BUTTON_MIDDLE:
                self.AddPage(
                    ".",
                    self.Stater.Export(
                        self.ButtonThread,
                        [
                            "Search",
                            "Content",
                            NewTitle,
                            {"claim_ids": ClaimIds},
                        ],
                    ),
                )
        elif self.ValueType == "channel":
            NewTitle = "Publications: " + self.Title
            if Event.button == Gdk.BUTTON_PRIMARY:
                threading.Thread(
                    target=self.ButtonThread,
                    args=(
                        [
                            "Search",
                            "Content",
                            NewTitle,
                            {"channel": self.Channel},
                        ]
                    ),
                ).start()
            elif Event.button == Gdk.BUTTON_MIDDLE:
                self.AddPage(
                    ".",
                    self.Stater.Export(
                        self.ButtonThread,
                        [
                            "Search",
                            "Content",
                            NewTitle,
                            {"channel": self.Channel},
                        ],
                    ),
                )

    def on_Link_clicked(self, Widget):
        if not Widget.get_realized():
            return
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        LBRYSettings = LBRYSettings["preferences"]["shared"]["value"]
        LinkCommand = LBRYSettings["LBRY-GTK"]["LinkCommand"]
        LinkSite = LBRYSettings["LBRY-GTK"]["LinkSite"]
        Link = self.Links.get_children()[LinkSite].get_uri()
        try:
            Open(Link, LinkCommand)
        except Exception as e:
            Error(error.error(e), self.Window)

    def on_RSS_clicked(self, Widget):
        if not Widget.get_realized():
            return
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return
        LBRYSettings = LBRYSettings["preferences"]["shared"]["value"]

        RSS = self.ChannelShort.replace("#", ":").split("lbry://", 1)[1]

        if LBRYSettings["LBRY-GTK"]["RSSSite"] == 0:
            Link = "https://odysee.com/$/rss/" + RSS
        else:
            Link = "https://lbry.bcow.xyz/" + RSS + "/rss"

        Gtk.Clipboard.set_text(
            Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD), Link, -1
        )

        Error("RSS link: " + Link + " copied to clipboard.", self.Window)

    def CreateInputWindow(self, Title, Session):
        Adjustment = Gtk.Adjustment.new(0, 0, 1000000000000, 1, 10, 0)
        SpinButton = Gtk.SpinButton.new(Adjustment, 0, 8)
        Dialog = Gtk.MessageDialog(
            self.Window,
            Gtk.DialogFlags.DESTROY_WITH_PARENT,
            buttons=Gtk.ButtonsType.OK_CANCEL,
        )
        Dialog.set_title(Title)
        ContentArea = Dialog.get_content_area()

        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(LBRYSettings, self.Window)
            return [0]
        LBRYSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]

        ChannelList = channel.channel_list(server=Session["Server"])
        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Channels.glade")
        Channelser = Channels(
            Builder,
            False,
            self.Window,
            ChannelList,
            LBRYSettings,
        )
        Builder.connect_signals(Channelser)
        ContentArea.add(Gtk.Label.new("Channel"))
        ContentArea.add(Channelser.Channels)
        ContentArea.add(Gtk.Label.new("Amount"))
        ContentArea.add(SpinButton)

        Dialog.show_all()
        Response = Dialog.run()
        Dialog.destroy()
        if Response == Gtk.ResponseType.OK:
            return [SpinButton.get_value(), Channelser.Get()]
        return [0]

    def on_Tip_clicked(self, Widget):
        if not Widget.get_realized():
            return
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        Value = self.CreateInputWindow("Tip", Session)
        if Value[0] != 0:
            JsonData = support.create(
                self.Data["claim_id"],
                Value[0],
                True,
                Value[1][0],
                Value[1][5],
                Session["Server"],
            )
            if isinstance(JsonData, str):
                Error(JsonData, self.Window)

    def on_Boost_clicked(self, Widget):
        if not Widget.get_realized():
            return
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        Value = self.CreateInputWindow("Boost", Session)
        if Value[0] != 0:
            JsonData = support.create(
                self.Data["claim_id"],
                Value[0],
                False,
                Value[1][0],
                Value[1][5],
                Session["Server"],
            )
            if isinstance(JsonData, str):
                Error(JsonData, self.Window)

    # Not Yet Implemented

    def on_Repost_clicked(self, Widget):
        This = "is not working yet"
