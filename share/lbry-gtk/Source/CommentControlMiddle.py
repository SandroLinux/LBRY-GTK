################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import threading, json, gi

gi.require_version("Gtk", "3.0")
from gi.repository import GLib

from flbry import comments

from Source import Places
from Source.Error import Error


class CommentControlMiddle:
    def __init__(self, *args):
        (
            self.Builder,
            self.Window,
            self.Row,
            self.Channelser,
            self.Edit,
            self.CommentServer,
            self.ReplyScrolled,
            self.Post,
            self.PreviewBox,
            self.Tip,
            self.TipLabel1,
            self.TipLabel2,
            self.PChannel,
            self.ChannelList,
        ) = args

        self.CommentID = self.Row[4]
        self.CommentControlMiddle = self.Builder.get_object(
            "CommentControlMiddle"
        )
        self.Heart = self.Builder.get_object("Heart")
        self.Like = self.Builder.get_object("Like")
        self.Dislike = self.Builder.get_object("Dislike")
        self.LikeNumber = self.Builder.get_object("LikeNumber")
        self.DislikeNumber = self.Builder.get_object("DislikeNumber")
        self.Reply = self.Builder.get_object("Reply")
        self.ChannelsBox1 = self.Builder.get_object("ChannelsBox1")

        self.Like.set_active(self.Row[10])
        self.Dislike.set_active(self.Row[11])
        if self.Row[12]:
            self.Heart.set_label("❤️")

        self.LikeNumber.set_label(" " + str(self.Row[8]) + " ")
        self.DislikeNumber.set_label(" " + str(self.Row[9]) + " ")

    def React(self, Channel, Type, Remove):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        ErrorOrData = comments.reaction_react(
            *Channel,
            self.CommentServer,
            str(self.CommentID),
            Type,
            Remove,
            server=Session["Server"]
        )
        if isinstance(ErrorOrData, str):
            Error(ErrorOrData, self.Window)
            return ""
        ErrorOrData = comments.reaction_list(
            *Channel,
            self.CommentServer,
            str(self.CommentID),
            server=Session["Server"]
        )
        if isinstance(ErrorOrData, str):
            Error(ErrorOrData, self.Window)
            return ""
        return ErrorOrData

    def on_Heart_clicked(self, Widget):
        Remove = Widget.get_label() == "❤️"
        threading.Thread(target=self.HeartThread, args=([Remove])).start()

    def HeartThread(self, Remove):
        for ChannelItem in self.ChannelList:
            if ChannelItem[3] == self.PChannel:
                Channel = [ChannelItem[0], ChannelItem[-1]]
                break
        Type = "creator_like"
        ErrorOrData = self.React(Channel, Type, Remove)
        if not isinstance(ErrorOrData, str):
            GLib.idle_add(self.HeartUpdate, Channel, ErrorOrData)

    def HeartUpdate(self, Channel, Data):
        Heart = (
            Data["my_reactions"][self.CommentID]["creator_like"] == 1
            or Data["others_reactions"][self.CommentID]["creator_like"] == 1
        )
        if Heart:
            self.Heart.set_label("❤️")
        else:
            self.Heart.set_label("💙")

    def on_LikeDislike_pressed(self, Widget):
        ChannelRow = self.Channelser.Get()
        Channel = [ChannelRow[0], ChannelRow[-1]]
        args = [Channel, Widget.get_name(), Widget.get_active()]
        threading.Thread(target=self.LikeDislikeThread, args=(args)).start()

    def LikeDislikeThread(self, Channel, Type, Remove):
        ErrorOrData = self.React(Channel, Type, Remove)
        if not isinstance(ErrorOrData, str):
            GLib.idle_add(self.LikeDislikeUpdate, Channel, ErrorOrData)

    def LikeDislikeUpdate(self, Channel, Data):
        self.Like.set_active(Data["my_reactions"][self.CommentID]["like"] == 1)
        self.Dislike.set_active(
            Data["my_reactions"][self.CommentID]["dislike"] == 1
        )
        self.LikeNumber.set_label(
            " "
            + str(
                Data["others_reactions"][self.CommentID]["like"]
                + Data["my_reactions"][self.CommentID]["like"]
            )
            + " "
        )
        self.DislikeNumber.set_label(
            " "
            + str(
                Data["others_reactions"][self.CommentID]["dislike"]
                + Data["my_reactions"][self.CommentID]["dislike"]
            )
            + " "
        )

    def on_Reply_clicked(self, Widget, Discard=""):
        if self.Edit.get_name() == "Edit":
            Visible = self.ReplyScrolled.get_visible()
            self.ReplyScrolled.set_visible(not Visible)
            self.Post.set_visible(not Visible)
            self.PreviewBox.set_visible(not Visible)
            self.Tip.set_visible(not Visible)
            self.TipLabel1.set_visible(not Visible)
            self.TipLabel2.set_visible(not Visible)
