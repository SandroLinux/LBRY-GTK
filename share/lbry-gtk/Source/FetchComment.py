################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, json, queue

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

from flbry import comments, settings, channel

from Source import Places
from Source.Comment import Comment
from Source.Error import Error


class FetchComment:
    def __init__(self, *args):
        (
            self.Window,
            self.ShowHiderTexts,
            self.GetPublication,
            self.Stater,
            self.AddPage,
            self.CommentExpander,
        ) = args

    def SingleComment(
        self,
        ClaimID,
        Box,
        Row,
        Boxes,
        CommentServer,
        HiddenUI,
        Start,
        Settings,
        ChannelList,
        Channel,
        RowQueue=queue.Queue(),
    ):
        CommentBuilder = Gtk.Builder.new_from_file(
            Places.GladeDir + "Comment.glade"
        )
        Commenter = Comment(
            CommentBuilder,
            self.ShowHiderTexts,
            ClaimID,
            Box,
            Row,
            Boxes,
            self.Window,
            self.SingleComment,
            self.GetPublication,
            HiddenUI,
            Start,
            CommentServer,
            Settings,
            ChannelList,
            self.Stater,
            self.AddPage,
            Channel,
            self.CommentExpander,
        )
        CommentBuilder.connect_signals(Commenter)
        if HiddenUI:
            return Commenter
        RowQueue.put(True)

    def DisplayPrice(self, Expander, ChannelID, CommentServer):
        CommentSettings = comments.setting_get(ChannelID, CommentServer)
        if isinstance(CommentSettings, str):
            return
        Amount = CommentSettings["min_tip_amount_comment"]
        if Amount != None:
            GLib.idle_add(self.UpdateExpander, Expander, Amount)

    def UpdateExpander(self, Expander, Amount):
        Expander.set_label("Comments (Tip " + str(Amount) + " LBC to comment)")

    def Fetch(self, ClaimID, CommentBox, ExitQueue, CommentServer, PChannel):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)

        Settings = settings.get(server=Session["Server"])
        if isinstance(Settings, str):
            Error(Settings, self.Window)
            return
        Settings = Settings["preferences"]["shared"]["value"]["LBRY-GTK"]

        Channel = ["", ""]
        ChannelList = channel.channel_list(server=Session["Server"])
        for ChannelItem in ChannelList:
            if ChannelItem[0] == Settings["CommentChannel"]:
                Channel = [ChannelItem[0], ChannelItem[-1]]

        CommentPerLoading = int(Settings["CommentPerLoading"])

        Boxes = {"Root": CommentBox}
        ToCheck = ["Root"]
        CommentID = ""
        Exit = False
        i = 0

        while i < len(ToCheck) and not Exit:
            try:
                Exit = ExitQueue.get(block=False)
                break
            except:
                pass
            CommentPlace = ToCheck[i]

            if CommentPlace != "Root":
                CommentID = CommentPlace

            CommentPage = 0

            PageNumber = 1
            PageIndex = 0

            while not Exit:
                try:
                    Exit = ExitQueue.get(block=False)
                    break
                except:
                    pass

                CommentPage += 1
                Box = Boxes[CommentPlace]

                Rows, PageNumber = comments.list(
                    ClaimID,
                    CommentServer,
                    parent_id=CommentID,
                    page_size=CommentPerLoading,
                    page=CommentPage,
                    server=Session["Server"],
                )

                if isinstance(Rows, str):
                    break

                CommentIDs = ""
                for Row in Rows:
                    CommentIDs += str(Row[4]) + ","

                Reactions = comments.reaction_list(
                    *Channel,
                    CommentServer,
                    CommentIDs,
                    server=Session["Server"]
                )

                if isinstance(Reactions, str):
                    break

                OtherReaction = Reactions["others_reactions"]
                try:
                    MyReaction = Reactions["my_reactions"]
                except:
                    pass

                RowQueue = queue.Queue()

                for Row in Rows:

                    Likes = OtherReaction[Row[4]]["like"]
                    Dislikes = OtherReaction[Row[4]]["dislike"]
                    Liked, Disliked = False, False
                    CreatorLiked = OtherReaction[Row[4]]["creator_like"] == 1

                    try:
                        CreatorLiked = (
                            CreatorLiked
                            or MyReaction[Row[4]]["creator_like"] == 1
                        )
                        Likes += MyReaction[Row[4]]["like"]
                        Dislikes += MyReaction[Row[4]]["dislike"]
                        Liked = MyReaction[Row[4]]["like"] == 1
                        Disliked = MyReaction[Row[4]]["dislike"] == 1
                    except:
                        pass

                    Row.extend([Likes, Dislikes, Liked, Disliked, CreatorLiked])

                    try:
                        Exit = ExitQueue.get(block=False)
                        break
                    except:
                        pass

                    GLib.idle_add(
                        self.SingleComment,
                        ClaimID,
                        Box,
                        Row,
                        Boxes,
                        CommentServer,
                        False,
                        False,
                        Settings,
                        ChannelList,
                        PChannel,
                        RowQueue,
                    )

                    if Row[0] != 0:
                        ToCheck.append(Row[4])

                for Row in Rows:
                    RowQueue.get()

                PageIndex += 1
                if isinstance(Rows, str) or PageIndex == PageNumber:
                    break

            i = i + 1
