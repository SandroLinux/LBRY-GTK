################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk


Types = [
    Gdk.ModifierType.MOD1_MASK,
    Gdk.ModifierType.CONTROL_MASK,
    Gdk.ModifierType.SUPER_MASK,
    Gdk.ModifierType.HYPER_MASK,
    Gdk.ModifierType.META_MASK,
]


def Convert(Text):
    Parts = Text.split(" ")
    return [Gdk.unicode_to_keyval(ord(Parts[1])), Types[int(Parts[0])]]


class KeyBind:
    def __init__(self, Builder):
        self.Builder = Builder
        self.KeyBind = self.Builder.get_object("KeyBind")
        self.Modifier = self.Builder.get_object("Modifier")
        self.Character = self.Builder.get_object("Character")

    def Get(self):
        return (
            str(self.Modifier.get_active()) + " " + self.Character.get_text()[0]
        )

    def Set(self, Text):
        Parts = Text.split(" ")
        self.Modifier.set_active(int(Parts[0]))
        self.Character.set_text(Parts[1])

    def get_text(self):
        return self.Get()

    def set_text(self, Text):
        self.Set(Text)
