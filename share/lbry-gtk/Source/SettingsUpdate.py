################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json, queue

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

from flbry import meta

from Source import Places, Settings, KeyBind
from Source.Error import Error


def UpdateWidth(Window, Session):
    Pager = Window.get_children()[0].get_children()[0]
    Length = Pager.get_n_pages()
    for Index in range(Length):
        Page = Pager.get_nth_page(Index)
        Label = Pager.get_tab_label(Page)
        Children = Label.get_children()
        Children[0].set_size_request(Session["PageWidth"], -1)
        Children[1].set_hexpand(Session["PageExpand"])


UpdateMetaQueue = queue.Queue()


def UpdateMeta(Inbox, Window):
    # Exit if user disabled Meta Service otherwise continue
    global UpdateMetaQueue
    threading.Thread(target=UpdateMetaThread, args=([Inbox, Window])).start()
    try:
        UpdateMetaQueue.get(block=False)
        return False
    except:
        return True


def UpdateMetaThread(Inbox, Window):
    # Check if user disabled Meta Service
    global UpdateMetaQueue
    with open(Places.ConfigDir + "Session.json", "r") as File:
        Session = json.load(File)
    if not Session["EnableMetaService"]:
        UpdateMetaQueue.put(True)
        return

    # Get Inbox
    LBRYSettings = Settings.Get()
    if isinstance(LBRYSettings, str):
        Error(LBRYSettings, Window)
        return
    LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"]["LBRY-GTK"]

    try:
        Notification = meta.list(
            LBRYGTKSettings["MetaServer"], LBRYGTKSettings["AuthToken"], True
        )[0]

        # Check if last notification is new
        with open(Places.CacheDir + "Notification.json", "r") as File:
            LastNotification = json.load(File)["LastNotification"]
        if LastNotification != Notification["id"]:
            GLib.idle_add(UpdateMetaUpdate, Inbox)
    except:
        pass


def UpdateMetaUpdate(Inbox):
    Inbox.set_active(True)


def ChangeMeta(Window, EnableMetaService):
    # Hide or show Icon
    TopPanel = (
        Window.get_children()[0]
        .get_children()[1]
        .get_children()[1]
        .get_children()[0]
    )
    Children = TopPanel.get_children()
    for Child in Children:
        try:
            if Child.get_label() == "🔔":
                Inbox = Child
                Child.set_visible(EnableMetaService)
                break
        except:
            pass

    # Start notification service
    if EnableMetaService:
        GLib.timeout_add(60 * 1000, UpdateMeta, Inbox, Window)
        threading.Thread(
            target=UpdateMetaThread, args=([Inbox, Window])
        ).start()


MinimumWidth = 0

MenuIcons = [
    "menu_new",
    "preferences-other",
    "image-loading",
    "document-properties",
    "applications-others",
]


def ChangeMenu(Window, Type, MenuIcon="", Remove=True):
    # Set MinimumWidth with full menu only once
    global MinimumWidth, MenuIcons
    if MinimumWidth == 0:
        MinimumWidth = Window.get_preferred_size().minimum_size.width

    TopPanel = (
        Window.get_children()[0]
        .get_children()[1]
        .get_children()[1]
        .get_children()[0]
    )

    Children = TopPanel.get_children()
    Menu = ""

    # Get all needed components
    for Child in Children:
        if isinstance(Child, Gtk.Box):
            try:
                Menu = Child.get_children()[0]
            except:
                pass
            MenuBox = Child
        if isinstance(Child, Gtk.MenuButton):
            Button = Child

    # Set icon
    Image = Button.get_children()[0]
    if MenuIcon != "" and Image.get_icon_name() != MenuIcons[MenuIcon]:
        Image.set_from_icon_name(MenuIcons[MenuIcon], Gtk.IconSize.BUTTON)

    # Do not change type if it's already that
    if TopPanel.get_name() != str(Type):
        TopPanel.set_name(str(Type))

        Popover = Button.get_popover()

        if Menu == "":
            Menu = Popover.get_children()[0]

        # Initial settings needed for each type
        Menu.unparent()
        MenuBox.foreach(MenuBox.remove)
        Popover.foreach(Popover.remove)
        Button.set_visible(Type == 2)
        WindowName = Window.get_name()

        # Do not remove function, if original type is adaptive
        if WindowName != "GtkWindow" and Remove:
            Window.disconnect(int(WindowName))
            Window.set_name("GtkWindow")

        # Add menu to the correct place
        # Adaptive adds it to the button, as function assumes it is somewhere
        if Type == 0:
            Window.set_name(str(Window.connect("configure-event", UpdateMenu)))
            Menu.set_pack_direction(Gtk.PackDirection.TTB)
            Popover.add(Menu)
            UpdateMenu(Window, "", Window.get_size().width)
        elif Type == 1:
            MenuBox.add(Menu)
            Menu.set_pack_direction(Gtk.PackDirection.LTR)
        elif Type == 2:
            Menu.set_pack_direction(Gtk.PackDirection.TTB)
            Popover.add(Menu)


def UpdateMenu(Window, Event, Width=""):
    # Setting MenuType depending on Window size
    if Width == "":
        Width = Event.width
    global MinimumWidth
    if Width < MinimumWidth:
        ChangeMenu(Window, 2, "", False)
    else:
        ChangeMenu(Window, 1, "", False)


def GetWindowWidgets(Main):
    return {
        "KBBack": [Main.Back.activate],
        "KBForward": [Main.Forward.activate],
        "KBAdd": [Main.Add.activate],
        "KBRefresh": [Main.Refresh.activate],
        "KBPreviousPage": [Main.Previous.activate],
        "KBNextPage": [Main.Next.activate],
        "KBLBRY-GTK": [Main.TopPaneler.LBRY.activate],
        "KBSearch": [Main.TopPaneler.Search.grab_focus],
        "KBInbox": [Main.TopPaneler.Inbox.activate],
        "KBMenu": [Main.TopPaneler.ShowMenu.activate],
        "KBNewPublication": [Main.TopPaneler.NewPublication.activate],
        "KBSettings": [Main.TopPaneler.Settings.activate],
        "KBHelp": [Main.TopPaneler.Help.activate],
        "KBStatus": [Main.TopPaneler.Status.activate],
        "KBAbout": [Main.TopPaneler.About.activate],
        "KBWallet": [Main.TopPaneler.Balance.activate],
        "KBHome": [Main.SidePaneler.Home.activate],
        "KBFollowing": [Main.SidePaneler.Following.activate],
        "KBYourTags": [Main.SidePaneler.YourTags.activate],
        "KBDiscover": [Main.SidePaneler.Discover.activate],
        "KBLibrary": [Main.SidePaneler.Library.activate],
        "KBCollections": [Main.SidePaneler.Collections.activate],
        "KBFollowed": [Main.SidePaneler.Followed.activate],
        "KBUploads": [Main.SidePaneler.Uploads.activate],
        "KBChannels": [Main.SidePaneler.Channels.activate],
    }


def GetPageWidgets(Page):
    return {
        "KBClose": [Page.CloseButton.activate],
        "KBScrollDown": [Page.ScrollDown.activate],
        "KBScrollUp": [Page.ScrollUp.activate],
        "KBMoveLeft": [
            Page.MoveLeft.activate,
            Page.Publicationer.MoveLeft.activate,
        ],
        "KBMoveRight": [
            Page.MoveRight.activate,
            Page.Publicationer.MoveRight.activate,
        ],
        "KBMoveUp": [Page.MoveUp.activate, Page.Publicationer.MoveUp.activate],
        "KBMoveDown": [
            Page.MoveDown.activate,
            Page.Publicationer.MoveDown.activate,
        ],
        "KBListOnCurrentPublication1": [
            Page.ListOpenOnCurrent.activate,
            Page.Publicationer.Activate1.activate,
        ],
        "KBListOnNewPublication2": [
            Page.ListOpenOnNew.activate,
            Page.Publicationer.Activate2.activate,
        ],
        "KBPublication3": [Page.Publicationer.Activate3.activate],
        "KBPlayOnCurrent": [
            Page.Publicationer.PublicationControler.PlayOnCurrent.activate
        ],
        "KBPlayOnNew": [
            Page.Publicationer.PublicationControler.PlayOnNew.activate
        ],
        "KBChannelOnCurrent": [
            Page.Publicationer.PublicationControler.ChannelOnCurrent.activate
        ],
        "KBChannelOnNew": [
            Page.Publicationer.PublicationControler.ChannelOnNew.activate
        ],
        "KBDownload": [
            Page.Publicationer.PublicationControler.Download.activate
        ],
        "KBLink": [Page.Publicationer.PublicationControler.Link.activate],
        "KBRepost": [Page.Publicationer.PublicationControler.Repost.activate],
        "KBBoost": [Page.Publicationer.PublicationControler.Boost.activate],
        "KBTip": [Page.Publicationer.PublicationControler.Tip.activate],
        "KBContentOnCurrent": [
            Page.Publicationer.PublicationControler.ContentOnCurrent.activate
        ],
        "KBContentOnNew": [
            Page.Publicationer.PublicationControler.ContentOnNew.activate
        ],
        "KBRSS": [Page.Publicationer.PublicationControler.RSS.activate],
        "KBFollow": [Page.Publicationer.PublicationControler.UnFollow.activate],
        "KBThumbnail": [Page.Publicationer.Thumbnailer.ThumbnailOpen.activate],
        "KBData": [Page.Publicationer.DataAction.activate],
        "KBTags": [Page.Publicationer.TagsAction.activate],
        "KBDescription": [Page.Publicationer.DescriptionAction.activate],
        "KBLinks": [Page.Publicationer.LinksAction.activate],
        "KBComments": [Page.Publicationer.CommentsAction.activate],
    }


def ShortCut(AccelGroup, Window, KeyVal, Modifier, Function):
    Function()


def CreateLambda(Function):
    return lambda A, B, C, D: ShortCut(A, B, C, D, Function)


def AcceleratorCreate(Window, Widgets, AccelGroup):
    Window.add_accel_group(AccelGroup)
    with open(Places.ConfigDir + "Session.json", "r") as File:
        Session = json.load(File)
    for WidgetName in Widgets:
        try:
            for Widget in Widgets[WidgetName]:
                AccelGroup.connect(
                    *KeyBind.Convert(Session[WidgetName]),
                    0,
                    CreateLambda(Widget)
                )
        except:
            pass


WindowAccelGroup = ""


def WindowAcceleratorCreate(Window, Main):
    global WindowAccelGroup
    Widgets = GetWindowWidgets(Main)
    if WindowAccelGroup != "":
        Window.remove_accel_group(WindowAccelGroup)
    WindowAccelGroup = Gtk.AccelGroup.new()
    AcceleratorCreate(Window, Widgets, WindowAccelGroup)


PageAccelGroup = ""


def PageAcceleratorCreate(Window, Page):
    global PageAccelGroup
    Widgets = GetPageWidgets(Page)
    if PageAccelGroup != "":
        Window.remove_accel_group(PageAccelGroup)
    PageAccelGroup = Gtk.AccelGroup.new()
    AcceleratorCreate(Window, Widgets, PageAccelGroup)
