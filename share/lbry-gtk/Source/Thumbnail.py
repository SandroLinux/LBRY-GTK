################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GdkPixbuf

from flbry import settings, error

from Source import Places, Image
from Source.Open import Open
from Source.Error import Error


class Thumbnail:
    Url, OldUrl, Width, Height = "", "", 0, 0

    def __init__(self, *args):
        self.Builder, self.Window = args
        self.Thumbnail = self.Builder.get_object("Thumbnail")
        self.ThumbnailOpen = self.Builder.get_object("ThumbnailOpen")
        Display = self.Thumbnail.get_display()
        self.DefaultCursor = Gdk.Cursor.new_from_name(Display, "default")
        self.PointerCursor = Gdk.Cursor.new_from_name(Display, "pointer")

    def on_ThumbnailOpen_activate(self, Widget):
        if not self.Thumbnail.get_realized():
            return
        Event = Gdk.Event.new(Gdk.EventType.BUTTON_PRESS)
        Event.button = Gdk.BUTTON_PRIMARY
        Event.window = self.Thumbnail.get_window()
        self.Thumbnail.event(Event)

    def on_Thumbnail_button_press_event(self, Widget, Event):
        ThumbnailSize = self.Thumbnail.get_allocated_size()[0]
        ThumbnailSize = [ThumbnailSize.width, ThumbnailSize.height]
        CursorPlace = [Event.x, Event.y]
        Image.GetUrlResolution(
            self.Url, self.IfIn, ThumbnailSize, CursorPlace, self.Press, [Event]
        )

    def Press(self, In, Event):
        if In:
            if Event.button != 8 and Event.button != 9:
                if self.Url == "":
                    Error(
                        "This publication does not have a thumbnail.",
                        self.Window,
                    )
                    return
                with open(Places.ConfigDir + "Session.json", "r") as File:
                    Session = json.load(File)
                Settings = settings.get(server=Session["Server"])
                if isinstance(Settings, str):
                    Error(Settings, self.Window)
                    return
                Settings = Settings["preferences"]["shared"]["value"][
                    "LBRY-GTK"
                ]
                ImageCommand = Settings["ImageCommand"]
                try:
                    Open(Image.Path(self.Url), ImageCommand)
                except Exception as e:
                    Error(error.error(e), self.Window)

    def on_Thumbnail_draw(self, Widget, Cairo):
        Size = Widget.get_parent().get_children()[1].get_allocated_size()[0]
        Width = Size.width
        Height = max(round(Width / 16 * 9), Size.height)
        Changed = "Nothing"

        if self.Width != Width or self.Height != Height:
            Widget.get_parent().set_size_request(-1, round(Width / 16 * 9))
            self.Pixbuf = GdkPixbuf.Pixbuf.new(
                GdkPixbuf.Colorspace.RGB, True, 8, Width, Height
            )
            self.Width, self.Height = Width, Height
            Changed = "Size"

        if self.OldUrl != self.Url:
            self.OldUrl = self.Url
            Changed = "Url"

        if Changed == "Url":
            Image.FillPixbuf(self.Url, self.Pixbuf, self.FinishDraw, Size)
        elif Changed == "Size":
            FilePath = ""
            if self.Url != "":
                FilePath = Image.Path(self.Url)
            Image.FillPixbufFill(FilePath, self.Pixbuf, self.FinishDraw, Size)
        else:
            Height = round((Size.height - self.Height) / 2)
            Gdk.cairo_set_source_pixbuf(Cairo, self.Pixbuf, 0, Height)
            Cairo.paint()

    def FinishDraw(self, Size):
        Height = round((Size.height - self.Height) / 2)
        Cairo = Gdk.cairo_create(self.Thumbnail.get_window())
        Gdk.cairo_set_source_pixbuf(Cairo, self.Pixbuf, 0, Height)
        Cairo.paint()

    def on_Thumbnail_motion_notify_event(self, Widget, EventMotion):
        ThumbnailSize = self.Thumbnail.get_allocated_size()[0]
        ThumbnailSize = [ThumbnailSize.width, ThumbnailSize.height]
        CursorPlace = [EventMotion.x, EventMotion.y]
        Image.GetUrlResolution(
            self.Url,
            self.IfIn,
            ThumbnailSize,
            CursorPlace,
            self.CursorUpdate,
            [],
        )

    def IfIn(self, ThumbnailSize, CursorPlace, Function, Args, PixbufSize):
        Ratios = []
        for Index in range(2):
            Ratios.append(ThumbnailSize[Index] / PixbufSize[Index])

        Ratio = min(*Ratios)
        FinalSize = [int(PixbufSize[0] * Ratio), int(PixbufSize[1] * Ratio)]

        HorizontalBounds = [(ThumbnailSize[0] - FinalSize[0]) / 2]
        HorizontalBounds.append(ThumbnailSize[0] - HorizontalBounds[0])

        VerticalBounds = [(ThumbnailSize[1] - FinalSize[1]) / 2]
        VerticalBounds.append(ThumbnailSize[1] - VerticalBounds[0])

        HorizontalBounds[0] < CursorPlace[0] < HorizontalBounds[1]

        Function(
            HorizontalBounds[0] < CursorPlace[0] < HorizontalBounds[1]
            and VerticalBounds[0] < CursorPlace[1] < VerticalBounds[1],
            *Args
        )

    def CursorUpdate(self, In, *Args):
        if In:
            self.Thumbnail.get_window().set_cursor(self.PointerCursor)
        else:
            self.Thumbnail.get_window().set_cursor(self.DefaultCursor)
