################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading

gi.require_version("Gtk", "3.0")
from gi.repository import Gdk

from Source.Error import Error


class SidePanel:
    def __init__(self, *args):
        (
            self.Builder,
            self.Window,
            self.NewPage,
            self.SamePage,
        ) = args
        self.SidePanel = self.Builder.get_object("SidePanel")
        self.Home = self.Builder.get_object("Home")
        self.Following = self.Builder.get_object("Following")
        self.YourTags = self.Builder.get_object("YourTags")
        self.Discover = self.Builder.get_object("Discover")
        self.Library = self.Builder.get_object("Library")
        self.Collections = self.Builder.get_object("Collections")
        self.Followed = self.Builder.get_object("Followed")
        self.Uploads = self.Builder.get_object("Uploads")
        self.Channels = self.Builder.get_object("Channels")

    def Check(self, Function, Event="", Button=""):
        if Button == "":
            Button = Event.button
        if self.Startuper.Started:
            if Button == Gdk.BUTTON_PRIMARY:
                self.SamePage(Function, [])
            elif Button == Gdk.BUTTON_MIDDLE:
                self.NewPage(Function, [])
        else:
            Error("LBRYNet is not running.", self.Window)

    def on_SidePanel_button_press_event(self, Widget, Event):
        self.Check(Widget.get_name(), Event)

    def on_SidePanel_activate(self, Widget):
        self.Check(Widget.get_name(), "", Gdk.BUTTON_PRIMARY)
