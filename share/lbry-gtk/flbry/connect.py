################################################################################
# LBRY-GTK Internals                                                           #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# This file will start the lbrynet sdk.

import subprocess, requests, time, queue


def start(
    function=print,
    exit_queue=queue.Queue(),
    binary="lbrynet start",
    timeout=30,
    server="http://localhost:5279",
):
    if check("", server):
        function(status(server))
    else:
        subprocess.Popen(
            binary,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.STDOUT,
            shell=True,
        )

    slept = 0

    while True:
        if check(function, server):
            break
        exit = False
        try:
            exit = exit_queue.get(block=False)
        except:
            pass
        if exit or timeout < slept:
            break
        sleep = 0.5
        time.sleep(sleep)
        slept += sleep

    return check("", server)


def stop(server="http://localhost:5279"):
    try:
        requests.post(server, json={"method": "stop"})
    except:
        pass
    if check("", server):
        return "LBRY Connection Closed"
    else:
        return "SDK is not running"


def status(server="http://localhost:5279"):
    try:
        return requests.post(server, json={"method": "status"}).json()[
            "result"
        ]["startup_status"]
    except:
        return {"blob_manager": False}


def check(function="", server="http://localhost:5279"):
    # This output true or false
    # whether the SDK is running

    gotstatus = status(server)

    started = 0
    for key in gotstatus.keys():
        if gotstatus[key]:
            started += 1

    if function != "":
        function(gotstatus)

    if started == len(gotstatus.keys()):
        return True

    return False


def check_timeout(function="", timeout=30, server="http://localhost:5279"):
    slept = 0

    while True:
        if check(function, server):
            break
        if timeout < slept:
            break
        sleep = 0.5
        time.sleep(sleep)
        slept += sleep

    return check("", server)
