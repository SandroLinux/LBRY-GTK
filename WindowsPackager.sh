#!/usr/bin/sh

################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium                                          #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

#Get all Msys2 dependencies

yes | pacman -S mingw-w64-x86_64-gtk3 mingw-w64-x86_64-python3 tar git unzip \
    mingw-w64-x86_64-gcc mingw-w64-x86_64-gdb mingw-w64-x86_64-python-pip \
    mingw-w64-x86_64-python3-gobject

#Get all Python dependencies

pip install requests Pillow appdirs pyinstaller markdown

#Build and install Setproctitle

pip download setproctitle
tar -xf setproctitle*.tar.gz
cd setproctitle*/.
python setup.py build --compiler=mingw32
python setup.py install
cd ..

#Fix icons (this will probably break Msys2 slightly)

wget http://deb.debian.org/debian/pool/main/a/adwaita-icon-theme/adwaita-icon-theme_3.22.0.orig.tar.xz
tar -C /mingw64/share/icons/ --strip-components=1 -xvf \
    adwaita-icon-theme_3.22.0.orig.tar.xz adwaita-icon-theme-3.22.0/Adwaita
gtk-update-icon-cache-3.0 /mingw64/share/icons/Adwaita

#Get LBRY-GTK and LBRYNet

git clone https://codeberg.org/MorsMortium/LBRY-GTK.git
wget https://github.com/lbryio/lbry-sdk/releases/download/v0.109.0/lbrynet-windows.zip
unzip lbrynet-windows.zip

#Put LBRYNet into LBRY-GTK

mv ./lbrynet.exe ./LBRY-GTK/

#Remove unneeded files

rm -rf ./setproctitle* lbrynet-windows.zip adwaita-icon-theme_3.22.0.orig.tar.xz

#Move lbry-gtk out of bin

mv ./LBRY-GTK/bin/lbry-gtk ./LBRY-GTK/

#At this point, LBRY-GTK works, but it still needs Msys2

#Package LBRY-GTK
#Cairo does not get imported anywhere, but it is used

pyinstaller --windowed --noconfirm --hidden-import cairo \
    --exclude-module ./LBRY-GTK/share/Source ./LBRY-GTK/lbry-gtk

#Copy extra files 

cp ./LBRY-GTK/lbrynet.exe ./dist/LBRY-GTK/

cp -r ./LBRY-GTK/share/* ./dist/LBRY-GTK/share/

#Remove unneeded icons from package

find ./dist/LBRY-GTK/share/icons/Adwaita/ -type f \
    -not -name 'image-missing*' -and -not -name 'go-next*' \
    -and -not -name 'go-previous*' -and -not -name 'list-remove*' \
    -and -not -name 'list-add*'  -and -not -name 'pan*' \
    -and -not -name 'view-refresh*' -and -not -name 'window-close*' \
    -and -not -name 'media-playback-start*' -and -not -name 'dialog-password*' \
    -and -not -name 'menu_new*' -and -not -name 'preferences-other*' \
    -and -not -name 'image-loading*' -and -not -name 'document-properties*' \
    -and -not -name 'applications-others*' -and -not -path '*places/*' -delete

#Remove unneeded language packs from package

rm -rf ./dist/LBRY-GTK/share/locale/*

#LBRY-GTK is available in the dist/LBRY-GTK folder
