# Rules of Contributing:

- Use PascalCase in Source, snake_case in flbry.
- When working on the GUI, use the Glade files as much as possible.
- When creating containers, use GtkBox instead of GtkGrid as much as possible.
- Run your code through black formatter, before commit. (black -l 80 ./)
- When all work is done, squash your commits, before merge.
